<?php

namespace Tests\Kata;

use Kata\BridgeLocation;
use Kata\Player;
use Kata\Island;
use Kata\BridgeLocationCanNotBeBuilt;
use PHPUnit\Framework\TestCase;

class BridgeLocationTest extends TestCase
{
    protected function setUp()
    {
        $this->dudu = new Player('dudu');
        $this->pelle = new Player('pelle');
    }

    /**
     * @test
     */
    public function it_can_be_built_if_location_is_empty()
    {
        $island1 = new Island('one');
        $island2 = new Island('two');

        $bridgeLocation = new BridgeLocation($island1, $island2);

        $bridgeLocation->build($this->dudu);

        $this->assertSame($this->dudu, $bridgeLocation->owner());
    }

    /**
     * @test
     */
    public function it_can_not_be_built_if_location_is_not_empty()
    {
        $island1 = new Island('one');
        $island2 = new Island('two');

        $bridgeLocation = new BridgeLocation($island1, $island2);


        $bridgeLocation->build($this->pelle);

        $this->expectException(BridgeLocationCanNotBeBuilt::class);
        $bridgeLocation->build($this->pelle);
    }
}
