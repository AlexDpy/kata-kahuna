<?php

namespace Tests\Kata;


use Kata\BridgeLocation;
use Kata\Island;
use Kata\Player;
use PHPUnit\Framework\TestCase;

class IslandTest extends TestCase
{
    /**
     * @var Player
     */
    private $dudu;

    /**
     * @var Island
     */
    private $lale;
    /**
     * @var BridgeLocation[]
     */
    private $emptyBridgeLocations;
    /**
     * @var BridgeLocation[]
     */
    private $builtBridgeLocations;

    private $huna;
    private $iffi;
    private $kahu;


    public function setUp()
    {
        $this->lale = new Island('Lale');
        $this->huna = new Island('Huna');
        $this->iffi = new Island('Hiffi');
        $this->kahu = new Island('Kahu');

        $this->pelle = new Player('pelle');
        $this->dudu = new Player('dudu');

        $this->emptyBridgeLocations = [
            new BridgeLocation($this->lale, $this->huna),
            new BridgeLocation($this->lale, $this->iffi),
        ];

        $laleKahu = new BridgeLocation($this->lale, $this->kahu);

        $laleKahu->build($this->pelle);
        $this->builtBridgeLocations = [
            $laleKahu,
        ];
    }

    /**
     * @test
     */
    public function it_should_have_empty_bridge_locations_with_other_islands()
    {
        $bridgeLocations = $this->lale->getEmptyBridgeLocations();

        $this->assertEquals(
            $this->emptyBridgeLocations,
            array_values($bridgeLocations)
        );
    }

    /**
     * @test
     */
    public function it_should_have_not_empty_bridge_locations_with_other_islands()
    {
        $bridgeLocations = $this->lale->getBuiltBridgeLocations();

        $this->assertEquals(
            $this->builtBridgeLocations,
            array_values($bridgeLocations)
        );
    }

    /**
     * @test
     */
    public function it_should_be_taken_by_a_player()
    {
        $this->assertNull($this->lale->getOwner());

        $this->emptyBridgeLocations[0]->build($this->pelle);

        $this->assertEquals(
            $this->pelle,
            $this->lale->getOwner()
        );
    }

    /**
     * @test
     */
    public function it_should_be_taken_by_the_other_player()
    {
        $this->assertNull($this->lale->getOwner());

        $this->emptyBridgeLocations[0]->build($this->dudu);
        $this->emptyBridgeLocations[1]->build($this->dudu);

        $this->assertEquals(
            $this->dudu,
            $this->lale->getOwner()
        );
    }

    private $pelle;
}
