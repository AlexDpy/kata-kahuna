# Kata PHP Starter

## Create the project

```bash
composer create-project -s dev alexdpy/kata-php-starter TheSuperKata && cd TheSuperKata
```

## Install the dependencies

```bash
make install
```

## Go!

To run your phpunit tests:
```bash
make test
```

To run the `kata.php` script:
```bash
make run
```
