<?php
/**
 * Created by PhpStorm.
 * User: pol
 * Date: 02/03/17
 * Time: 15:32
 */

namespace Kata;


class BridgeLocation
{
    /**
     * @var Island
     */
    private $island1;

    /**
     * @var Island
     */
    private $island2;

    /**
     * @var boolean
     */
    private $owner;

    /**
     * BridgeLocation constructor.
     *
     * @param Island $island1
     * @param Island $island2
     */
    public function __construct(Island $island1, Island $island2)
    {
        $this->island1 = $island1;
        $this->island2 = $island2;

        $this->island1->addBridgeLocation($this);
        $this->island2->addBridgeLocation($this);

        $this->owner = null;
    }

    /**
     * @param Player $owner
     * @throws BridgeLocationCanNotBeBuilt
     */
    public function build(Player $owner)
    {
        if ($this->isBuilt()) {
            throw new BridgeLocationCanNotBeBuilt();
        }
        $this->owner = $owner;
    }

    /**
     * @return bool
     */
    public function isBuilt()
    {
        return $this->owner !== null;
    }

    /**
     * @return Player|null
     */
    public function owner()
    {
        return $this->owner;
    }
}
