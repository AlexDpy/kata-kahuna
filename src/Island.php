<?php
/**
 * Created by PhpStorm.
 * User: pol
 * Date: 02/03/17
 * Time: 15:27
 */

namespace Kata;


class Island
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var BridgeLocation[]
     */
    private $bridgeLocations;

    public function addBridgeLocation(BridgeLocation $bridgeLocation)
    {
        $this->bridgeLocations[] = $bridgeLocation;
    }

    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return BridgeLocation[]
     */
    public function getEmptyBridgeLocations()
    {
        return array_filter($this->bridgeLocations, function (BridgeLocation $bridgeLocation) {
            return !$bridgeLocation->isBuilt();
        });
    }

    public function getBuiltBridgeLocations()
    {
        return array_filter($this->bridgeLocations, function (BridgeLocation $bridgeLocation) {
            return $bridgeLocation->isBuilt();
        });
    }

    /**
     * @return Player|null
     */
    public function getOwner()
    {
        $halfCountOfBridgeLocations = count($this->bridgeLocations) / 2;

        foreach ($this->getBridgeLocationsOwners() as $owner) {
            if (count($this->getBuildBridgeLocationsBy($owner)) > $halfCountOfBridgeLocations) {
                return $owner;
            }
        }
        return null;
    }

    private function getBuildBridgeLocationsBy(Player $owner)
    {
        return array_filter($this->getBuiltBridgeLocations(), function(BridgeLocation $bridgeLocation) use ($owner){
            return $bridgeLocation->owner() === $owner;
        });
    }

    private function getBridgeLocationsOwners()
    {
        $owners = [];
        foreach ($this->getBuiltBridgeLocations() as $bridgeLocation) {
            $owner = $bridgeLocation->owner();
            $owners[$owner->getName()] = $bridgeLocation->owner();
        }
        return array_values($owners);
    }
}
